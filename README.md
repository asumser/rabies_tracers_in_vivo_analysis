# rabies_tracers_in_vivo_analysis

Analysis code and data for ELife publication:
"Fast, high-throughput production of improved rabies viral vectors for specific, efficient and versatile transsynaptic retrograde labeling"


Matlab code for calculating direction selectivity and make plots for the in vivo figures based on previously exported data from 3 mice.

## Authors and acknowledgment
Anton Sumser

## License
GNU GENERAL PUBLIC LICENSE Version 3

